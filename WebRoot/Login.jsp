<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'Login.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  <%
    //读取ie里面保存的cookie信息
    Cookie[] cookies =request.getCookies();
    String userName = "";
    String userPwd = "";
    if(cookies!=null){
    for(Cookie ck:cookies){
    	if(ck.getName().equals("userName")){
    		userName = ck.getValue();
    	}
    	if(ck.getName().equals("userPwd")){
    		userPwd = ck.getValue();
    	}
    }
    }
   %>
  <form method="post" action="doLogin.jsp">
    <table width="500px" align="center">
    <tr><td colspan="2">请登录系统</td></tr>
    <tr><td>用户名：</td><td><input type="text" name="userName" value="<%=userName%>"></td></tr>
     <tr><td>密码：</td><td><input type="password" name="userPwd" value="<%=userPwd%>"></td></tr>
     <tr><td colspan="2"><input type="checkbox" name="chkRemember" value="y">记住我</td></tr>
      <tr><td colspan="2"><input type="submit" value="登录"></td></tr>
   
    </table> <br>
   </form>
  </body>
</html>
