<%@ page language="java" import="java.util.*" pageEncoding="GB18030"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'VisitCount.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
    <%
    	//默认访问量=count
            Object obj =application.getAttribute("count");
            int count = 0;
        try {
    		if (obj == null) {
    			count = 1;
    		} else {
    			count = Integer.parseInt(obj.toString()) + 1;
    		}
    	} catch (Exception ex) {

    	}
		application.setAttribute("count", count);
    	//如果是第一个访问，就设置为1
    	//否则就取出数据，加1,
    	//将访问量信息放入application
    %>
    当前访问量:<%=count %>
  </body>
</html>
