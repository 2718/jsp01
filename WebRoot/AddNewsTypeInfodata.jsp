<%@ page language="java" import="java.util.*,model.*,dao.*" pageEncoding="GB18030"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'AddNewsTypeInfodata.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
   <%
    //request.setCharacterEncoding("utf-8");
    
    String typeName = new String(request.getParameter("typeName").getBytes("ISO-8859-1"),"utf-8");
    String remark = new String(request.getParameter("remark").getBytes("ISO-8859-1"),"utf-8");
    NewsTypeInfo nt = new NewsTypeInfo();
		nt.setTypeName(typeName);
		nt.setRemark(remark);
		 NewsTypeInfoDao dao = new NewsTypeInfoDao();
		if(dao.add(nt)>0){
			//新增成功，自动跳转到列表页面
			response.sendRedirect("Register.jsp");
		}else{
			out.print("数据新增失败,"+dao.msg+"。<a href='AddNewsTypeInfo.jsp'>返回</a>");
		}
     %>
    
  </body>
</html>
