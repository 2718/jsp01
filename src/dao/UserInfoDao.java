package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import org.junit.Test;

public class UserInfoDao {
	//进入数据库查询数据
	//完成登陆功能
	//使用unit tEST的要求：方法必须是public，void，无参
	DbDao dbDao = new DbDao();
	@Test
	public void doLogin(){
		//先从控制台获取用户名和密码
		Scanner sc = new Scanner(System.in);
		String userName = sc.next();
		String userPwd = sc.next();
		//进入数据库查询
		//1.1导入对应的数据库jar包,加载驱动
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String url = "jdbc:mysql://localhost:3306/newsdb";
		String user = "root";
		String password = "root";
		Connection con=null;
		try {
			//get connection
			con = DriverManager.getConnection(url, user, password);
			//get statement
			Statement st = con.createStatement();
			//get user login sql
			String sql = "select * from userinfo where userName='"+userName+"'";
			sql += " and userpwd='"+userPwd+"'";
			ResultSet rs = st.executeQuery(sql);
			if(rs.next()){
				System.out.println("登陆成功");
			}else{
				System.out.println("用户名或密码错误，请重新登陆");
			}
			rs.close();
			st.close();
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		 
	}
	public boolean doLogin(String userName,String userPwd){
		boolean ifLogin=false;
		//load class 
		String sql = "select * from userinfo where userName=? and userPwd=?";
		ResultSet rs =dbDao.execute(sql, userName,userPwd);
		try {
			if(rs.next()){
				ifLogin = true;
			}else{
				ifLogin = false;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			//dbDao.close();
		}
		return ifLogin;
	}
}
