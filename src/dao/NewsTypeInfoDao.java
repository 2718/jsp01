package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;

import model.NewsInfo;
import model.NewsTypeInfo;

public class NewsTypeInfoDao {
	DbDao dbDao = new DbDao();
	public static String msg="";
	public List<NewsTypeInfo> selectAll(){
		List<NewsTypeInfo> list = null;
		ResultSet rs = null;
		String sql = "";
		sql = "select * from newstypeinfo";
		rs = dbDao.execute(sql);
		try {
			list = new ArrayList<NewsTypeInfo>();
			while(rs.next()){
				//完成数据的赋值
				NewsTypeInfo ni=new NewsTypeInfo();
				ni.setId(rs.getInt("id"));
				ni.setTypeName(rs.getString("typeName"));				 
				//加入到List
				list.add(ni);				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			dbDao.res = rs;
			dbDao.close();
		}
		return list;
	}
	public int add(NewsTypeInfo nt){
		int result =0;
		String sql = " insert into NewsTypeInfo(typeName,remark) values(?,?)";
		try {
			result=dbDao.executeSQL(sql,nt.getTypeName(),nt.getRemark());			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			msg = e.getMessage();
			e.printStackTrace();
		} 
		return result;
	}
}
