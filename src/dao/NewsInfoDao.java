package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.NewsInfo;
import model.NewsTypeInfo;

public class NewsInfoDao {
	DbDao daoUtil = new DbDao();
	public String msg;
	public int addNews(int typeId,String title,String contents){
		int result =0;
		String sql = " insert into NewsInfo(typeId,title,contents,publishDate) values(?,?,?,now())";
		try {
			daoUtil.executeSQL(sql,typeId,title,contents);			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			msg = e.getMessage();
			e.printStackTrace();
		} 
		return result;
	}
	//因为resultset与数据库的强关联性，一般要将数据转移到集合里面
	public List<NewsInfo> selectAll(){
		List<NewsInfo> list = null;
		ResultSet rs = null;
		String sql = "";
		sql = "select n.id,t.typename,n.title,n.publishDate from newsinfo n join newstypeinfo t on n.typeId = t.id";
		rs = daoUtil.execute(sql);
		try {
			list = new ArrayList<NewsInfo>();
			while(rs.next()){
				//完成数据的赋值
				NewsInfo ni=new NewsInfo();
				ni.setId(rs.getInt("id"));
				ni.setTitle(rs.getString("title"));
				NewsTypeInfo nt=new NewsTypeInfo();
				nt.setTypeName(rs.getString("typeName"));
				ni.setNewsTypeInfo(nt);
				//加入到List
				list.add(ni);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			daoUtil.res = rs;
			daoUtil.close();
		}
		return list;
	}
}
