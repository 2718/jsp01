package dao;
import java.io.*;
import java.lang.reflect.Field;
import java.sql.*;
import java.util.Properties;
public class DbDao {
	private static String db_driver = "com.mysql.jdbc.Driver";//数据库驱动
	private static String db_url = "jdbc:mysql://localhost:3306/newsdb?userUnicode=true&characterEncoding=utf-8";//连接字符串
	private static String db_userName = "root";//用户名
	private static String db_userPass = "root";//用户密码
	private static String db_state = "";//状态
	private static String db_dataBaseName = "";//数据库名
	
	private static Connection conn = null;//连接对象
	private static PreparedStatement pst = null;//预编译对象
	private static CallableStatement cs= null;
	public static ResultSet res = null;//结果集对象
	private static Statement st = null;	
	
	/**
	 * 创建数据库连接
	 * @return
	 */
	public static Connection getConn(){		
		try {
			Class.forName(db_driver);
			conn = java.sql.DriverManager.getConnection(db_url, db_userName, db_userPass);
			//conn.setAutoCommit(false);		//关闭自动提交功能，改为人工提交事务
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}
	
	/**
	 * 关闭数据库参数
	 */
	public static void close(){
		try {
			if(res != null){
				res.close();
			}
			if(pst != null){
				pst.close();
			}
			if(st != null){
				st.close();
			}
			if(cs != null){
				cs.close();
			}
			if(conn != null){
				conn.close();
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 执行sql语句的增删改
	 * @param sql
	 * @param param
	 * @return
	 */
	public static Integer executeSQL(String sql,Object... param) throws SQLException{
		Integer result = 0;
		conn = null;
		pst = null;
		try {
			conn = getConn();
			pst = conn.prepareStatement(sql);
			for(int i=0;i<param.length;i++){
				pst.setObject(i+1, param[i]);
			}
			result = pst.executeUpdate();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			
			e.printStackTrace();
		}finally{
			close();
		}
		return result;
	}
	/**
	 * 普通sql查询
	 * @param sql
	 * @param param
	 * @return
	 */
	public static ResultSet execute(String sql,Object... param){
		try {
			conn = getConn();
			pst = conn.prepareStatement(sql);
			for(int i=0;i<param.length;i++){
				pst.setObject(i+1, param[i]);
			}
			res = pst.executeQuery();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return res;
	}
	/**
	 * 反射实现的新增操作
	 * @param  clssname——完整的类名信息
	 * @param param
	 * @return
	 */
	public void insert(Object obj) throws Exception {  
	      
	    try {  
	        conn = getConn();  
	        Class  tableClass = obj.getClass();
	        Field[] fields = tableClass.getFields();  
	        
	        // 下面一段代码准备SQL语句的两部分。  
	        StringBuilder sb1 = new StringBuilder();  
	        StringBuilder sb2 = new StringBuilder();  
	  
	        for (int i = 0; i < fields.length; i++) {  
	            if(i>0) {  
	                sb1.append(",");  
	                sb2.append(",");  
	            }  
	            sb1.append(fields[i].getName());  
	            sb2.append("?");  
	        }  
	  
	        String commaSeparatedFieldNames = sb1.toString();  
	        String commaSeparatedQuestionMarks = sb2.toString();  
	        
	        
	        // 安全起见，我们需要用prepareStatement处理用户输入。  
	        // 但是因为类的名称是可以由程序员控制的，我们用String.format生成语句  
	        pst = conn.prepareStatement(String.format(  
	                    "INSERT INTO %s(%s) values(%s)",  
	                    tableClass.getSimpleName(), commaSeparatedFieldNames,  
	                    commaSeparatedQuestionMarks));  
	  
	        // 然后，填充这个PreparedStatement  
	        for (int i = 0; i < fields.length; i++) {  
	            pst.setObject(i + 1, fields[i].get(obj));  
	        }  
	  
	        pst.executeUpdate();  
	    } finally {  
	        close();
	    }  
	}  
}
