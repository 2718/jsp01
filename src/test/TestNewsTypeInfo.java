package test;

import java.util.List;

import model.NewsTypeInfo;

import org.junit.Test;

import dao.NewsTypeInfoDao;

public class TestNewsTypeInfo {
	NewsTypeInfoDao dao = new NewsTypeInfoDao();
	@Test
	public void selectAll(){
		List<NewsTypeInfo> list =dao.selectAll();
		for(NewsTypeInfo nt:list){
			System.out.println(nt.getId()+":"+nt.getTypeName());
		}
	}
	@Test
	public void add(){
		NewsTypeInfo nt = new NewsTypeInfo();
		nt.setTypeName("科技前沿");
		nt.setRemark("最新资讯");
		if(dao.add(nt)>0){
			System.out.println("新增成功");
		}else{
			System.out.println(dao.msg);
		}
	}
}
