package test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import model.NewsInfo;

import org.junit.Test;

import dao.NewsInfoDao;
import dao.UserInfoDao;

public class TestNewsInfo {
	NewsInfoDao nDao = new NewsInfoDao();
	@Test
	public void addNews(){
		int typeId=2;
		String title = "文章打人";
		String contents = "泡夜店";		
		int i = 0;
		i = nDao.addNews(typeId, title, contents);
		if(i>=0){
			System.out.println("新增成功");
		}else{
			System.out.println("新增失败,原因是"+nDao.msg);
		}
	}
	@Test
	/*public void selectAll(){
		ResultSet rs = nDao.selectAll();
		try {
			while(rs.next()){
				System.out.println(rs.getInt("id")+" "+rs.getString("typeName")+" "+rs.getString("title")+rs.getDate("publishDate"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		UserInfoDao udao = new UserInfoDao();
		if (udao.doLogin("admin", "1")){
			System.out.println("欢迎来到优衣库");
		} else{
			System.out.println("请重新登陆");
			}
	}*/	
	public void selectAll(){
		List<NewsInfo> list = nDao.selectAll();
		for(NewsInfo ni:list){
			System.out.println(ni.getId()+":"+ni.getTitle()+":"+ni.getNewsTypeInfo().getTypeName());
		}
	}
}
