package model;

import java.sql.Timestamp;

 

public class NewsInfo {
	private int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getContents() {
		return contents;
	}
	public void setContents(String contents) {
		this.contents = contents;
	}
	public Timestamp getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(Timestamp publishDate) {
		this.publishDate = publishDate;
	}
	public NewsTypeInfo getNewsTypeInfo() {
		return newsTypeInfo;
	}
	public void setNewsTypeInfo(NewsTypeInfo newsTypeInfo) {
		this.newsTypeInfo = newsTypeInfo;
	}
	private String title;
	private String contents;
	private Timestamp publishDate;
	private NewsTypeInfo newsTypeInfo;//表里面有外键关联的时候，定义实体类的时候，就直接将外键关联的类定义进来
}
